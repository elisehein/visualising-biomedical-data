# Visualising biomedical data

This project manifests itself as a javascript heavy single-page web application that
condenses data from PubMed and a local database and visualises it aiming to
aid scannability. It is built on Flask, with Backbone running the
front-end.

## Running the application

**Requirements**: mysql, python2.7

### Setting up the database

Follow these steps to set up the db locally.

### 1. Create a new database `epidemiology_db`

	$ mysql -u <username> -p
	> create database epidemiology_db;

### 2. Load the dump into the new database. From the project root directory:

	$ less db_dump.sql | mysql -u <username> -p -D epidemiology_db
	
### 3. Add your database credentials to `config.py`

Edit the file `config.py` in the project root directory with the username and password to access the database. You can also change the default database name.

## Setting up virtualenv
	
Virtualenv is used to create a virtual environment for the application code
to run in so that you don't clutter your own system with unnecessary
libraries and dependencies. See https://pypi.python.org/pypi/virtualenv for
more information.

To install virtualenv with pip:

	$ pip install virtualenv
	
Otherwise see the installation instructions on https://pypi.python.org/pypi/virtualenv
(the installation includes pip; useful for the next step)
	
Activate virtualenv in the project root directory:

	$ virtualenv ENV
	$ source ENV/bin/activate
	
All of the libraries that this project needs to run are listed in
`requirements.txt`. In order to install these libraries in the currently
running virual environment only, use

	$ pip install -r requirements.txt
	
Everything is now set up to run the application.

## Running the application

There are two options to run the app.

1. Run `$ python runserver.py` from `/code`. The website is now running on
	 127.0.0.1:5000
2. Run `$ gunicorn -w 3 --debug --preload puff:app` from `/code`. The
	 website is now running on 127.0.0.1:8000


	

	





