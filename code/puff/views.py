from puff import app
import json
from api import display_related_entities
from flask import render_template, request, jsonify
from utils import SERVER_NAME, SERVER_PORT
from pattern.en import pluralize

@app.route('/')
def home():
    page_title = 'Welcome!'
    return render_template('pages/welcome.html', page_title=page_title);

@app.route('/search')
def search_page():
    page_title = 'Search results'
    return render_template('pages/search.html', page_title=page_title)

@app.route('/help')
def help_page():
    page_title = 'Guidelines'
    return render_template('pages/help.html', page_title=page_title)
    
@app.route('/about')
def about_page():
    page_title = 'About'
    return render_template('pages/about.html', page_title=page_title)
    
