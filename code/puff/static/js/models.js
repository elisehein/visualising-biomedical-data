// ARTICLES
// ==================================================================

Article = Backbone.Model.extend({
	parse: function(resp, xhr) {
		relevant_info = {};
		relevant_info['author'] = resp['FAU'];
		relevant_info['title'] = resp['TI'];
		relevant_info['abstr'] = resp['AB'];
		relevant_info['date'] = resp['DP'];
		relevant_info['pmid'] = resp['PMID'];
		relevant_info['journal'] = resp['JT'];
		relevant_info['issue'] = resp['IS'];
		relevant_info['tags'] = resp['MH'];
		return relevant_info;
		},
	defaults: {
		author: '',  // FAU
		title: '',   // TI
		abstr: '',   // AB
		date: '',    // DP
		pmid: '',    // PMID
		journal: '', // JT
		issue: '', 	 // IS
		tags: '',    // MH
		},
	initialize: function(props) {
		this.url = props.url;
		}
	});

Articles = Backbone.Collection.extend({
	model: Article,
	});

// THE CLOUD
// ==================================================================

// An entity is one of the resulting related entities of the search
// These are created based on the SearchParameters Collection
RelatedEntity = Backbone.Model.extend({
	defaults: {
		epidem_category: '',
		value: '',
		freq: 0,
		other_details: {},
		is_filter: false,
		initially_hidden: true,
		}
});

RelatedEntities = Backbone.Collection.extend({
	model: RelatedEntity,
	url: '/api/entities/related',
	parse: function(resp, xhr) {
		this.article_ids = resp.article_ids;
		var response = resp.related;
		
		this_view = this;
		// Process each entity to add more properties
		_.each(response, function(e) {
			
			// Decide if this entity was used to filter the search
			var is_filter = this_view.detect_filters(e['epidem_category'], e['value']);
			e['is_filter'] = is_filter;
			
			// Decide if this entity should be initially hidden
			// Only exposures and outcomes should be visible initially
			if (e['epidem_category'] == 'outcome' || e['epidem_category'] == 'exposure' || e['is_filter']) {
				e['initially_hidden'] = false;
				}
			});
		
		return response;
		},
	initialize: function() {
		filters.bind('all', this.decide_fetch, this);
		},
	decide_fetch: function() {
		// Manual caching of related entity data
		// If we can find the current url in the localStorage.backboneCache object,
		// use it, triggering appropriate events along the way
		
		// Build current url;
		var qs = this.build_data();
		var current_url = this.url + '?' + qs;
		
		// Check if it's cached
		var is_cached = false;
		var cache = eval('(' + localStorage.backboneCache + ')');
		if (cache && qs != '' && current_url in cache) {
			// Disable all of this for now and just go straight to 'else'
			// since the problem seems to be svg rendering, adding this only makes it worse with large sets
			//is_cached = true;
			}
		
		// If cached, populate this collection with cached data
		if (is_cached) {
			console.log("Data is cached; begin silent retrieval");
			 
			// Trigger fetch event to indicate loading
			this.trigger('fetch');
			
			// Populate
			var cached_obj = cache[current_url]['value'];
			this.article_ids = cached_obj['article_ids'];
			
			// Empty the collection silently
			this.reset(undefined, {silent: true});
			
			// Add each related entity silently
			var entity_models = [];
			_.each(cached_obj['related'], function(m) {
				var entity_model = new RelatedEntity(m);
				entity_models.push(entity_model);
				});
			this.add(entity_models, {silent: true});
			
			// Trigger reset event to draw the cloud
			this.trigger('reset');
			}
		else {
			this.refetch();
			}
		},
	refetch: function() {
		this.fetch({
			data: this.build_data(), 
		  // This is for the caching 
			// It returns the correct data (I modified the fetch-cached library)
			// But breaks functionality
			//prefill: true,
			//prefillSuccess: function(data) {
			//	console.log("prefill success");
			//	console.log(data);
			//	},
			//cache: true
			});
		},
	build_data: function() {
		// The data object should be built based on the current filters
		var data = URI('?');
		filters.each(function(filter) {
			var filter_cat = filter.get('is_keyword') 
			             ? 'keyword'
			             : filter.get('epidem_category');
			var filter_val = filter.get('value');
			data.addSearch(filter_cat, filter_val);
			});
		return data.query();
		},
	get_by_cat : function(cat) {
		models = [];
		var model = this.detect(function(model) {
			if (model.get('epidem_category') == cat) {
				models.push(model);
				}
			});
		return models;
		},
	detect_filters: function(cat, val) {
		var is_filter = false;
		
		filters.each(function(filter) {
			
			var filter_cat = '';
			if (filter.get('epidem_category') == 'studydesign') { 
				filter_cat = 'type_of_study';
				}
			else if (filter.get('epidem_category') == 'effectsize') { 
				filter_cat = 'effect_size_concept'; 
				}
			else { 
				filter_cat = filter.get('epidem_category');
				}
			
			if (filter_cat == cat && filter.get('value') == val) {
				is_filter = true;
				}
			});
		return is_filter;
		}
	});

// HiddenEntities needs to be just like RelatedEntities, 
// but is not allowed to fetch data
HiddenEntities = Backbone.Collection.extend({
	model: RelatedEntity,
	get_by_cat : function(cat) {
		models = [];
		var model = this.detect(function(model) {
			if (model.get('epidem_category') == cat) {
				models.push(model);
				}
			});
		return models;
		}
	});


// FILTERING
// ==================================================================

// A SearchParameter is an attribute to the search the user is performing
// eg outcome: obesity, or retmax=10
SearchParameter = Backbone.Model.extend({
	// Possible types: 
	defaults: {
		is_keyword: false,
		epidem_category: '',     // Not needed for retmax
		value: '',               // Not needed for requested_category
		},
	});

SearchParameters = Backbone.Collection.extend({
	model: SearchParameter,
	// Enable filtering parameters
	byEpidemCategory: function(cat){
		filtered = this.filter(function(param){
			return param.get('epidem_category') === cat;
			});
		return new SearchParameters(filtered)
		}
	});



// STEAMGRAPHS
// ==================================================================

ArticleFrequencies = Backbone.Model.extend({
	url: '/api/articles/years',
	initialize: function() {
		related_entities.bind("reset", this.remove);
		},
	});

EntityFrequencies = Backbone.Model.extend({
	url: '/api/entities/years',
	initialize: function(params) {
		related_entities.bind("reset", this.remove);
		this.value = params.value;
		this.epidem_category = params.epidem_category;
		},
	});
