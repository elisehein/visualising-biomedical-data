// This is the modal that pops up when I click see context
ContextModal = Backbone.View.extend({
	tagName: 'div',
	className: 'reveal-modal xlarge context-modal',
	template: _.template($('#see-context-template').html()),
	initialize: function(props) {
		related_entities.bind('fetch', this.remove, this);
		this.concept = props.concept;
		this.identifier = props.identifier;
		$(this.el).attr('id', 'context-' + this.identifier);
		this.render();
		$(this.el).reveal({
			animation: 'fade',
			animationspeed: 100,
			});
		},
  render: function() {
		$(this.el).html(this.template);
		this.$('h3 span').text('"' + this.concept + '"');
		this.fetch_sentences();
		$('body').append(this.el);
		},
	fetch_sentences: function() {
		// Fetch the sentences
		var context;
		var this_view = this;
		$.ajax({
			url: '/api/entity/context',
			type: 'POST',
			data: {'concept': this.concept, 'article_ids': related_entities.article_ids.toString()},
			dataType: 'json',
			success: function(data) {
				context = data['context']
				this_view.add_sentences(context);
				},
			});
		},
	add_sentences: function(context) {
		// Stop loading
		this.$('.loading').remove();
		
		var sentences = this.process_sentences(context);
		var this_view = this;
		if (sentences != []) {
			_.each(sentences, function(s) {
				this_view.$('ul.context').append('<li>' + s + '</li>');
				});
			}
		else {
			this_view.$('ul.context').append('<span class="no-sentences">No matching sentences</span>');
			}
		},
	process_sentences: function(context) {
		// Process the sentences
		
		// Prepare regex; adapted from
		// http://stackoverflow.com/questions/494035/how-do-you-pass-a-variable-to-a-regular-expression-javascript
		var escape_string = function(s) {
		  return s.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
		  };
		
		var sentences = [];
		_.each(context, function(sentence) {
			
			// Remove newlines
			var trimmed = $.trim(sentence['sentence']).replace(new RegExp('\n', 'g'), ' ');
			
			// Wrap the concept in a span, if not already in one
			var escaped_highlight = escape_string(sentence['highlight']);
			var regex = new RegExp('[^>]?' + escaped_highlight, 'gi');
			var highlighted_sentence = trimmed.replace(regex, 
				                                         '<span class="highlight">'
																								 + sentence['highlight']
																								 + '</span>')
				                                          
			// Add the appropriate classname
			highlighted_sentence = highlighted_sentence.replace('class="highlight',
				                                                  'class="highlight ' 
																													+ sentence['highlight_cat'])
			
			sentences.push(highlighted_sentence);
			});
		return sentences;
		},
	});

// This view represents a context menu for an entity in the cloud
CloudMenu = Backbone.View.extend({
	tagName: 'div',
	className: 'cloud-menu',
	template: _.template($('#cloud-menu-template').html()),
	events: {
		'click .filter-search': 'filter_search',
		'click .new-search': 'new_search',
		'click .hide-entity': 'hide_entity',
		'click .add-steam': 'add_steam',
		'click .see-context': 'show_context',
		'click .browse-external': 'browse_external'
		},
	initialize: function() {
		this.id = this.model.cid;
		this.render();
		},
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		this.set_text();
		$(this.el).attr('id', this.id);
		this.disable_options_for_filters();
		$('body').append(this.el);
		},
	set_text: function() {
		var cat = '';
		if (this.model.get('epidem_category') == 'type_of_study') {
			cat = 'Type of study';
			}
		else if (this.model.get('epidem_category') == 'effect_size_concept') {
			cat = 'Effect size';
			}
		else {
			cat = this.model.get('epidem_category');
			}
		
		this.$('.value').text(this.model.get('value'));
		this.$('.epidem-category').text(cat);
		
		var umls_group = this.model.get('other_details')['umls_group'];
		var umls_category = this.model.get('other_details')['umls_category'];
		if (umls_group) {
			this.$('.umls-group').text(umls_group);
			}
		else {
			this.$('.umls-group').remove();
			}
		},
	filter_search: function() {
		$('.cloud-menu#' + this.model.cid).hide();
		this.add_filter();
		},
	new_search: function() {
		$('.cloud-menu#' + this.model.cid).hide();
		filters.reset();
		this.add_filter();
		},
	hide_entity: function() {
		$('.cloud-menu#' + this.model.cid).hide();
		$('.cloud').css('opacity', 1);
		$('text#' + this.model.cid).fadeOut(200, function() { entity_cloud.update_counts() });
		related_entities.remove(this.model);
		},
	add_steam: function() {
		var is_disabled = false;
		if (this.$('.add-steam').hasClass('disabled')) {
			is_disabled = true;
			}
		
		if (!is_disabled) {
			// Scroll to steamgraphs
			$('html, body').animate({
			 scrollTop: $(".steam-view").offset().top
			 }, 300);
			
			// Re-render steamgraph
			var cat = this.model.get('epidem_category');
			if (cat == 'type_of_study') {
				cat = 'studydesign';
				}
			if (cat == 'effect_size_concept') {
				cat = 'effectsize';
				}
			steamgraphs.add_entity(cat, this.model.get('value'));
			
			// Remove option to add the same graph again
			this.$('.add-steam').addClass('disabled');
			steamgraphs.disabled_cids.push(this.model.cid);
			}
		},
	show_context: function() {
		// Open a modal with a list of sentences where this term occurs in
		if ($('.context-modal#context-' + this.model.cid).length) {
			$('.context-modal#context-' + this.model.cid).reveal({
				animation: 'fade',
				animationspeed: 100,
				});
			}
		else {
			this.add_context_view()
			}
		},
	browse_external: function() {
		// Search Google scholar for this entity
		var url = 'https://scholar.google.co.uk/scholar?q=';
		url += this.model.get('value');
		window.open(url, '_blank');
		window.focus();
		},
	add_filter: function() {
		var is_disabled = false;
		if (this.$('.add-steam').hasClass('disabled')) {
			is_disabled = true;
			}
		
		if (!is_disabled) {
			// Because the related_entities models come straight from the
			// api endpoint, the naming for ep.cats is inconsistent
			var cat = this.model.get('epidem_category');
			if (cat == 'type_of_study') {
				cat = 'studydesign';
				}
			if (cat == 'effect_size_concept') {
				cat = 'effectsize';
				}
			
			filters.add(new SearchParameter({
				epidem_category: cat,
				is_keyword: false,
				value: this.model.get('value')
				}));
			}
		},
	add_context_view: function(sentences) {
		new_view = new ContextModal({concept: this.model.get('value'),
		                             identifier: this.model.cid});
		},
	disable_options_for_filters: function() {
		if (this.model.get('is_filter')) {
			this.$('.add-steam').addClass('disabled');
			this.$('.filter-search').addClass('disabled');
			}
		},
	});

// This view represents a listing for a hidden entity 
// Can be unhidden, in which case a matching DOM element is shown,
// or if not found, the cloud is redrawn.
HiddenEntity = Backbone.View.extend({
	tagName: 'li',
	className: 'hidden-entity',
	template: _.template($('#hidden-entity-template').html()),
	events: {
		'click': 'unhide'
		},
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		this.set_text();
		return this.el;
		},
	set_text: function() {
		var cat = this.model.get('epidem_category');
		if (cat == 'type_of_study') {
			cat = 'type of study';
			}
		if (cat == 'effect_size_concept') {
			cat = 'effect size';
			}
		var val = this.model.get('value');
		this.$('.epidem-category').text(cat);
		this.$('.value').text(val);
		},
	unhide: function() {
		related_entities.add(this.model);
		
		// If we haven't redrawn the cloud yet and the element is just hidden,
		// simply show it again
		if ($('text#' + this.model.cid).length) {
			$('text#' + this.model.cid).fadeIn(200);
			}
		// If we have already redrawn the cloud without the hidden element,
		// need to redraw again
		else {
			entity_cloud.render();
			}
		},
	});

// The Cloud view does not have a tagname or any add events
// When changes occur to the SearchParams, it simply has to update everything
Cloud = Backbone.View.extend({
	el: '.cloud-view',
	events: {
		'click text': 'show_menu',
		'click .redraw': 'render',
		'click .legend span': 'toggle_all_from_cat'
		},
	initialize: function() {
		this.hidden_entities = new HiddenEntities;
		this.category_visibilities = {
			outcome: true, 
			covariate: true, 
			exposure: true, 
			population: true, 
			studydesign: true, 
			effectsize: true
			};
		
		// When a new filter is added, reset loading 
		related_entities.bind("fetch", this.init_loading, this);
		
		// When a new set of entities is retrieved,
		// render a new cloud and initialize the hidden collection
		related_entities.bind("reset", this.init_cloud, this);
		
		// Manage the hidden entities collection
		related_entities.bind("remove", this.add_to_hidden, this);
		related_entities.bind("add", this.remove_from_hidden, this);
		
		// Fetch our entities
		related_entities.decide_fetch();
		},
	init_loading: function() {
		$('.cloud .no-results').remove();
		this.show_loading();
		this.reset_loading_feedback();
		},
	init_cloud: function() {
		this.init_hidden();
		this.init_counts();
		this.remove_initially_hidden();
		this.render();
		},
	build_cloud_data: function() {
		
		// Normalise the font sizes
		var normalize = this.normalize;
		
		// Build a data object to be feeded to the cloud renderer
		var cloud_data = related_entities.map(function(e){ 
			var val = e.get('value');
			var max_len = 21;
			if (val.length > max_len) {
				val = val.substring(0, max_len) + '...';
				}
			return {
				id: e.cid,
				text: val,
				size: normalize(parseInt(e.get('freq'))),
				epidem_category: e.get('epidem_category'),
				other_details: e.get('other_details'),
				is_filter: e.get('is_filter'),
				} 
			});
			return cloud_data;
		},
	remove_initially_hidden: function() {
		// Remove all entities from related_entities that are initially_hidden
		var models_to_remove = [];
		related_entities.each(function(e) {
			if (e.get('initially_hidden')) {
				models_to_remove.push(e);
				}
			});
		related_entities.remove(models_to_remove);
		},
	render: function() {
		// Need to make sure the cloud is empty before re-rendering
		$('.cloud svg').fadeOut(200, function() { $(this).remove() });
		$('.cloud-menu').remove();
		$('.cloud .no-results').remove();
		$('.warning').empty();
		this.show_loading();
		
		var cloud_data = this.build_cloud_data();
		
		// Let users know when there are no results
		if (related_entities.length == 0) {
			this.hide_loading();
			$('.cloud').prepend('<span class="no-results">No results.</span>');
			}
		else {
			// Create context menus
			this.build_menus();
			
			// Build cloud
			this.cloud_width = this.$('.cloud').width();
			this.cloud_height = 470;
				
			var this_view = this;
			d3.layout.cloud()
			.size([this.cloud_width, this.cloud_height])
			.words(cloud_data)
			.timeInterval(1)
			.rotate(function(d) { return this_view.set_rotation(d.epidem_category, d.is_filter) })
			.font('Gentium Book Basic')
			.fontSize(function(d) { return d.size; })
			.on('end', this.draw_cloud)
			.start();
			
			// Render hidden_entities
			this.render_hidden_entities();
			}
		},
	draw_cloud: function(words) {
		// Need to figure out how to reference this view from inside this function
		// right now using entity_cloud global variable
		
		// Bring back full colors
		$('.cloud').css('opacity', 1);
		
		var canvas = d3.select('.cloud').append("svg")
		.attr("width", entity_cloud.cloud_width)
		.attr("height", entity_cloud.cloud_height)
		.append("g")
		.attr("transform", "translate(" + (entity_cloud.cloud_width/2) + "," + (entity_cloud.cloud_height/2) + ")")
		
		var placeholders = canvas
		.selectAll("text")
		.data(words)
		.enter()
		.append("text")
		.style("font-size", function(d) { return d.size + "px"; })
		.style("font-family", 'Gentium Book Basic')
		.style("fill", function(d, i) { return entity_cloud.set_color(d.epidem_category);})
		.attr("text-anchor", "middle")
		.attr("transform", function(d) {
			return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
		})
		.attr("class", function(d) { return d.epidem_category })
		.attr("id", function(d, i) { return d.id })
		.text(function(d) { return d.text; })
		
		// Set fill color to lighter on hover
		var original_fill = '';
		$('.cloud text').hover(function(){
			original_fill = d3.select(this).style('fill');
			d3.select(this).style('fill', d3.rgb(original_fill).brighter());
			}, function() {
			d3.select(this).style('fill', original_fill);
			});
		
		// Update loaded and total counts
		entity_cloud.update_counts();
		
		// Check how many fit on canvas
		entity_cloud.check_canvas_fit();
		
		// Hide loader
		entity_cloud.hide_loading();
		
		},
	set_color: function(cat) {
		// Current pallette from http://www.colourlovers.com/palette/1580423/Flames_at_sea
		// http://www.colourlovers.com/palette/559428/lucky_bubble_gum
		var color_outcome = '#43777a';
		var color_exposure = '#d95b45';
		var color_covariate = '#442432';
		var color_population = '#c02948';
		var color_studydesign = '#b38d19';
		var color_effectsize = '#807f87';
		
		switch(cat) {
			case 'outcome':
				return color_outcome;
			case 'exposure':
				return color_exposure;
			case 'covariate':
				return color_covariate;
			case 'type_of_study':
				return color_studydesign;
			case 'effect_size_concept':
				return color_effectsize;
			case 'population':
				return color_population;
			default:
				return color_outcome;
			}
		},
	check_canvas_fit: function() {
		// Check if all terms will fit on the canvas
		// If not, show a message
		if (d3.selectAll('.cloud text')[0].length < related_entities.length) {
			this.$('.warning').html('<div><i class="icon-warning-sign"> </i>This search has too many related terms to display everything. Filter your search or hide some terms to see more. <i class="icon-remove close"></i></div>');
			}
		
		// Add option to remove this message
		this.$('.warning .close').click(function() {
			$('.warning div').slideUp(100);
			});
		},
	set_rotation: function(given_category, is_filter) {
		rotation = 0;
		
		if (!is_filter) {
			switch(given_category) {
				case 'effect_size_concept':
					rotation = 90;
					break;
				case 'outcome':
					rotation = 0;
					break;
				case 'exposure':
					rotation = 3;
					break;
				case 'covariate':
					rotation = 5;
					break;
				case 'population':
					rotation = -90;
					break;
				case 'type_of_study':
					rotation = -3;
					break;
				default:
					rotation = 0;
				}
			}
		
		return rotation;
		},
	normalize: function(given_size) {
		
		// Construct a new linear scale
		frequencies = related_entities.map(function(e) { 
													return parseInt(e.get('freq'));
												});
		scale = d3.scale.pow().exponent(.3)
			.domain(d3.extent(frequencies))    // Given frequencies are mapped on to
			.range([0, 35]);             // a given font size range
		
		
		// Set required font size based on the number of terms in the cloud
		var no_terms = related_entities.length;
		var base_size = 13;
		if (no_terms < 15) { base_size = 30; }
		else if (no_terms < 30) { base_size = 25; }
		else if (no_terms < 50) { base_size = 20; }
		else if (no_terms < 65) { base_size = 16; }
		
		// Return size given by the scale added to the base_size
		return scale(given_size) + base_size;
		},
	build_menus: function() {
		related_entities.each(function(entity) {
			var new_menu = new CloudMenu({model: entity});
			});
		},
	show_menu: function(e) {
		var menu = $('.cloud-menu#' + e.target.id);
		
		// Ensure that we are hovering the menu when it appears
		menu.css('top', e.pageY - 15);
		menu.css('left', e.pageX - 15);
		
		menu.hover(function() {
			$('.cloud').css('opacity', 0.5);
			}, function() {
			menu.hide();
			$('.cloud').css('opacity', 1);
			});
		
		$('.cloud').css('opacity', 0.4);
		menu.show();
		},
	init_hidden: function() {
		// Keep track of words we don't want to see
		this.hidden_entities.reset();
		$('.hidden-entities .hidden-entity').remove();
		$('.hidden-menu').hide();
		this.$('.legend span').attr('class', 'checked');
		},
	init_counts: function() {
		$('.legend > span i').attr('class', 'icon-check');
		// Also set initial category_counts that are NOT hidden
		// this should never be affected by (un)hiding categories
		// as it reflects the whole results set
		this.count = related_entities.length;
		this.$('h3 .loaded').text('');
		this.$('h3 .total').text('');
		this.category_counts = {
			outcome: related_entities.get_by_cat('outcome').length,
			exposure: related_entities.get_by_cat('exposure').length,
			covariate: related_entities.get_by_cat('covariate').length,
			population: related_entities.get_by_cat('population').length,
			studydesign: related_entities.get_by_cat('type_of_study').length,
			effectsize: related_entities.get_by_cat('effect_size_concept').length,
			};
		
		this.$('.legend #outcome span').text('(' + this.category_counts['outcome'] + ')');
		this.$('.legend #exposure span').text('(' + this.category_counts['exposure'] + ')');
		this.$('.legend #covariate span').text('(' + this.category_counts['covariate'] + ')');
		this.$('.legend #population span').text('(' + this.category_counts['population'] + ')');
		this.$('.legend #studydesign span').text('(' + this.category_counts['studydesign'] + ')');
		this.$('.legend #effectsize span').text('(' + this.category_counts['effectsize'] + ')');
		
		// Indicate which categories have no terms
		_.each(this.category_counts, function(count, cat) {
			if (count == 0) {
				$('.legend #' + cat).attr('class', '');
				$('.legend #' + cat).addClass('unchecked disabled');
				$('.legend #' + cat + ' i').attr('class', 'icon-check-empty');
				}
			});
		},
	update_counts: function() {
		this.$('h3 .loaded').text($('.cloud text:visible').length);
		this.$('h3 .total').text(entity_cloud.count);
		},
	add_to_hidden: function(hidden_model) {
		this.hidden_entities.add(hidden_model);
		this.adjust_visibility_states(hidden_model.get('epidem_category'));
		this.render_hidden_entities();
		},
	remove_from_hidden: function(unhidden_model) {
		this.hidden_entities.remove(unhidden_model);
		this.adjust_visibility_states(unhidden_model.get('epidem_category'));
		this.render_hidden_entities();
		},
	toggle_all_from_cat: function(event) {
		// Terms of a category are considered to be collectively hidden 
		// when all terms of that category are hidden 
		// If at least one term of a category is visible, 
		// that category is collectively visible (but visually, we can see the intermediate state)
		
		this.show_loading();
		
		// Need to define a separate cat_db_field 
		// because related entities are fetched straight from the db 
		// and naming is inconsistend
		var cat_db_field = event.target.id;
		if (cat_db_field == 'studydesign') { cat_db_field = 'type_of_study'; }
		if (cat_db_field == 'effectsize') { cat_db_field = 'effect_size_concept'; }
		var cat = event.target.id;
		
		// Store this cloud view
		this_view = this;
		
		// If terms of this category are visible, hide them
		if(this.category_visibilities[cat]) {
			// It's enough to just hide the text shapes for now because we've saved them to hidden
			$('text.' + cat_db_field).fadeOut(200, function() { this_view.update_counts(); });
			
			// Remove all related entities of this type from related_entities
			models_to_remove = related_entities.get_by_cat(cat_db_field);
			related_entities.remove(models_to_remove);
			}
		// If terms in this category are hidden, show them
		else {
			// Add these entities back to the main collection
			models_to_add = entity_cloud.hidden_entities.get_by_cat(cat_db_field);
			related_entities.add(models_to_add);
			
			// Show these entities
			var all_exist_in_dom = true;
			_.each(models_to_add, function(model) {
				if (!$('text#' + model.cid).length) {
					all_exist_in_dom = false;
					}
				});
				
			// If all of them have dom elements, just fadeIn
			if (all_exist_in_dom) {
				_.each(models_to_add, function(model) {
					$('text#' + model.cid).fadeIn(200);
					setTimeout(function() { this_view.update_counts(); }, 300);
					});
				}
			// Otherwise, re-render the whole cloud
			else {
				this_view.render();
				}
			}
		
		this.hide_loading();
		},
	render_hidden_entities: function() {
		// Empty current hidden list
		$('.hidden-entities .hidden-entity').remove();
		$('.hidden-menu').hide();
		
		// If we have hidden entities, 
		// add a view for each
		if (this.hidden_entities.length){
			this.hidden_entities.each(function(entity) {
				var view = new HiddenEntity({model: entity});
				$('.hidden-entities').append(view.render());
				});
			$('.hidden-menu').show();
			}
		},
	adjust_visibility_states: function(cat_db_field) {
		// Here, the argument is in the correct db naming format
		var cat = cat_db_field;
		if (cat == 'type_of_study') { cat = 'studydesign'; }
		if (cat == 'effect_size_concept') { cat = 'effectsize'; }
		var hidden_count = this.hidden_entities.get_by_cat(cat_db_field).length;
		
		// If we have at least one visible entity of this category,
		// it's state is visible
		this.category_visibilities[cat] = true; 
		if (hidden_count == 0) { 
			$('.legend #' + cat).attr('class', 'checked');
			$('.legend #' + cat + ' i').attr('class', 'icon-check');
			}
		else if (hidden_count < this.category_counts[cat]) {
			$('.legend #' + cat).attr('class', 'indeterminate');
			$('.legend #' + cat + ' i').attr('class', 'icon-check');
			}
		// If all entities of this category are hidden
		else {
			this.category_visibilities[cat] = false;	
			$('.legend #' + cat).attr('class', 'unchecked');
			$('.legend #' + cat + ' i').attr('class', 'icon-check-empty');
			}
		},
	show_loading: function() {
		$('.cloud svg').css('opacity', 0.5);
		$('.cloud .loading').show();
		},
	hide_loading: function() {
		$('.cloud svg').css('opacity', 1);
		$('.cloud .loading').hide();
		},
	reset_loading_feedback: function() {
		$('.cloud-view .loaded').text('');
		$('.cloud-view .total').text('');
		},
	});

