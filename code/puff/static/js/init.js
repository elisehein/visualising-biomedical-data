$(document).ready(function() {
	// Initialize search parameters and filters collections
	filters = new SearchParameters(get_param_models());

	// Fire up the routing
	router = new RouteUpdater;

	// Fetch the related entities collection
	related_entities = new RelatedEntities;
	
	// Initialize views
	filters_list = new FiltersList;
	entity_cloud = new Cloud;
	articles_list = new ArticlesList;
	steamgraphs = new Steamgraphs;

	 
	// Parse the current querystring and build SearchParameter models based on it
	function get_param_models() {
		var given_qs = URI(location.hash.substring(location.hash.indexOf('?'))).search(true);
		var param_models = [];

		$.each(given_qs, function(k, v) {
			var is_keyword = false;
			if (k == 'keyword') { 
				is_keyword = true; 
				k = '';
				}
			if (v instanceof Array) {
				$.each(v, function(i) {
					param_models.push(new SearchParameter({
						epidem_category: k, 
						value: v[i], 
						is_keyword: is_keyword
						}));
					});
				}
			else {
				param_models.push(new SearchParameter({
					epidem_category: k, 
					value: v, 
					is_keyword: is_keyword
					}));
				}
			});
		return param_models;
		}
	
	});


// Make fetch() trigger an event so I can show loading statuses
// code from http://tbranyen.com/post/how-to-indicate-backbone-fetch-progress

// Patch Model and Collection.
 _.each(["Model", "Collection"], function(name) {
   // Cache Backbone constructor.
	 var ctor = Backbone[name];
	 // Cache original fetch.
	 var fetch = ctor.prototype.fetch;
			 
	 // Override the fetch method to emit a fetch event.
	 ctor.prototype.fetch = function() {
		 // Trigger the fetch event on the instance.
		 this.trigger("fetch", this);
                     
		 // Pass through to original fetch.
		 return fetch.apply(this, arguments);
		 };
	 });
