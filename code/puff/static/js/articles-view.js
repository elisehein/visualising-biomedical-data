// This is the view for a single row in the epidemiology facts table 
EpidemTableRow = Backbone.View.extend({
	tagName: 'div',
	className: 'epidem-table-row',
	template: _.template($('#article-facts-template').html()),
	initialize: function() {
		$(this.el).html(this.template);
		
		var label = this.options.label;
		if (label == 'effectsizes') { label = 'Effect sizes'; }
		if (label == 'studydesigns') { label = 'Types of study'; }
		this.$('.label').text(label);
		
		this_view = this;
		_.each(this.options.entities, function(e) {
			var val = e['value'];
			var umls_group = 'umls_group' in e ? e['umls_group'] : '';
			var other = e['other_value'];
			
			var entity_template = '<div class="entity"><span class="value">' + val + '</span>'
			entity_template += umls_group != '' ? '<span class="umls-group">' + umls_group + '</span>' : '';
			entity_template += other != '' ? '<span class="other">' + other + '</span>' : '';
			entity_template += '</div>';
			
		  this_view.$('.entities').append(entity_template);
			});
		},
	});

// This is the view for the modal that pops up when you click on an article
ArticleModal = Backbone.View.extend({
	tagName: 'div',
	className: 'reveal-modal',
	template: _.template($('#article-details-template').html()),
	events: {
		'click .pubmed-link span': 'open_pubmed_listing'
		},
	initialize: function() {
		related_entities.bind("fetch", this.remove, this);
		$(this.el).attr('id', 'modal-' + this.model.cid);
		$(this.el).addClass('xlarge');
		this.render();
		
		// Fetch facts for this article
		this.get_facts();
		},
	render: function() {
		$(this.el).html(this.template);
		this.set_text();
		this.$('.abstr').expander({
			slicePoint: 1000,
			widow: 50,
			expandText: 'Show full',
			expandEffect: 'slideDown',
			collapseEffect: 'slideUp',
			userCollapseText: 'Show less'
			});
		this.init_accordion();
		$('body').append(this.el);
		},
	set_text: function() {
		var title = this.model.get('title');
		var author = this.model.get('author');
		var date = this.model.get('date');
		var journal = this.model.get('journal');
		var issue = this.model.get('issue');
		var pmid = this.model.get('pmid');
		
		this.$('.title').text(title);
		this.$('.author').append(author.join('; '));
		this.$('.date').append(date);
		this.$('.journal span:nth-child(2)').append(journal);
		this.$('.issue span:nth-child(2)').append(issue);
		this.$('.pmid span:nth-child(2)').append(pmid);
		
		this.highlight_abstr();
		},
	highlight_abstr: function() {
		// First get the abstract itself
		var abstr = this.model.get('abstr');
		
		// Then fetch the highlights
		var highlights = '';
		$.ajax({
			url: '/api/article/' + this.model.get('pmid') + '/highlights',
			dataType: 'json',
			async: false,
			success: function(data) {
				highlights = data['highlights'];
				}
			});
		
		// Highlight abstract
		var highlighted_abstr = abstr;
		
		// Prepare regex; adapted from
		// http://stackoverflow.com/questions/494035/how-do-you-pass-a-variable-to-a-regular-expression-javascript
		var escape_string = function(s) {
		  return s.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
		  };
		
		// Wrap each highlight into a *single* span tag
		var this_view = this;
		_.each(highlights, function(highlight) {
			 
			// If it's not yet wrapped, wrap it
			var escaped_highlight = escape_string(highlight['concept']);
			// Only matches strings that don't have a span tag before them (to avoid double wrapping)
			var regex = new RegExp('[^>]' + escaped_highlight, 'g');
			highlighted_abstr = highlighted_abstr
			.replace(regex, 
							 '<span class="highlight">'
							 + highlight['concept'] 
							 + '</span>');
		
			});
		
		// Update the dom
		this_view.$('.abstr').html(highlighted_abstr);
			 
		// Add the appropriate classname
		// For some reason need to do this in a separate loop
		// for the dom to update
		_.each(highlights, function(highlight) {
			var term_span = this_view.$('.abstr span.highlight:contains("' + highlight['concept'] + '")');
			var tooltip_so_far = term_span.attr('title');
			var tooltip_str = tooltip_so_far ? tooltip_so_far + ', ' + highlight['class'] : highlight['class'];
			term_span.addClass(highlight['class']).attr('title', tooltip_str);
			});
			
		},
	get_facts: function() {
		// Show that we're loading some stuff
		this.$('.facts').html('<i class="icon-spinner icon-spin"></i>');
		
		// Get table with all entities mentioned in this article
		var this_view = this;
		$.ajax({
			url: '/api/article/' + this.model.get('pmid') + '/facts',
			dataType: 'json',
			success: function(data) {
				this_view.build_facts_table(data);
				}
			});
		},
	build_facts_table: function(data) {
		this.$('.facts').html('');
		
		// Add a table row for each epidem type we have data about
		var this_view = this;
		_.each(data, function(entities, epidem_category) {
			if (entities.length) {
				var new_table_row = new EpidemTableRow({
					label: epidem_category,
					entities: entities,
					});
				this_view.$('.facts').append(new_table_row.el);
				}
			});
		},
	init_accordion: function() {
		var this_view = this;
		this.$('.section-heading').click(function() {
			if ($(this).hasClass('open')) {
				// Do nothing
				}
			else {
				$(this_view.el).find('.section-content').slideUp('normal');
				this_view.$('.section-heading').find('i').attr('class', 'icon-angle-right');
				this_view.$('.section-heading').removeClass('open');
				$(this).next().slideDown('normal');
				$(this).find('i').attr('class', 'icon-angle-down');
				$(this).addClass('open');
				}
			});
		this.$('.section-content:has(.facts)').hide();
		},
	open_pubmed_listing: function() {
		var url = 'http://www.ncbi.nlm.nih.gov/pubmed/';
		url += this.model.get('pmid');
		window.open(url, '_blank');
		window.focus();
		}
	});

// This is the view for an individual article listing
SingleArticle = Backbone.View.extend({
	tagName: 'li',
	className: 'article-listing',
	template: _.template($('#article-listing-template').html()),
	events: {
		'click .title': 'show_article_info',
		},
	initialize: function() {
		related_entities.bind("fetch", this.remove, this);
		$(this.el).attr('id', this.model.cid);
		this.render();
		},
	// Render this article listing
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		this.set_text();
		$('#articles-list').append(this.el);
		this.modal = new ArticleModal({model: this.model});
		},
	set_text: function() {
		var title = this.model.get('title');
		
		// Sometimes the title is wrapped in square brackets
		// ending with a '.'
		if (title[0] == '[') {
			title = title.substring(1, title.length-2);
			}
		var date = this.model.get('date');
		this.$('.title').prepend(title);
		this.$('.date').append(date);
		},
	show_article_info: function() {
		// Reveal the modal
		$(this.modal.el).reveal({
			animation: 'fade',
			animationspeed: 100,
			});
		},
	});

SingleArticleError = Backbone.View.extend({
	tagName: 'li',
	className: 'article-listing-error',
	template: _.template($('#article-listing-error-template').html()),
	events: {
		'click .try-again': 'try_again'
		},
	initialize: function() {
		related_entities.bind('fetch', this.remove, this);
		},
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		this.set_text();
		return this;
		},
	set_text: function() {
		this.$('.article-id').text(this.model.id);
		},
	try_again: function() {
		console.log("attempting to fetch again");
		this.$('.try-again').text('Attempting to fetch again...');
		new_article = new Article({
			url: '/api/article/' + this.model.id + '/about',
			id: this.model.id
			});
		new_article.fetch({
			success: _.bind(function(article) {
				console.log("succeeded new fetch");
				this.replace_with_article_view(article);
				}, this),
			error: _.bind(function(article) {
				this.$('.try-again').text('Try again');
				}, this)
			});
		},
	replace_with_article_view: function(article) {
		$(this.el).slideUp(300, function() { var view = new SingleArticle({model: article}); });
		$(this.el).remove();
		}
	});

ArticlesList = Backbone.View.extend({
	el: $('#articles-list'),
	articles_loaded: 0,
	articles_amt: 0,
	load_step: 5,
	events: {
		'scroll': 'detect_scroll'
		},
	initialize: function() {
		related_entities.bind("fetch", this.reset_loading_feedback, this);
		related_entities.bind("reset", this.render, this);
		},
	render: function() {
		// Reset some parameters
		this.articles_loaded = 0;
		this.articles_amt = related_entities.article_ids.length;
		
		// Fetch just enough articles to be able to scroll
		// Assume that one article takes up about 50px vertically
		var loaded_height = 0;
		var single_height = 50;
		while (loaded_height - 100 < $(this.el).height()) {
			// If we run out before we reach the bottom, 
			// break loop
			if (this.articles_loaded + 1 > this.articles_amt) {
				this.update_loading_feedback();
				break;
				}
			this.fetch_some_articles(1);
			loaded_height += single_height;
			}
		},
	fetch_some_articles: function(step) {
		var ceiling = this.articles_loaded + step;
		if (this.articles_amt < ceiling) {
			ceiling = this.articles_amt;
			}
		for (var i = this.articles_loaded; i < ceiling; i++) {
			new_article = new Article({
				url: '/api/article/' + related_entities.article_ids[i] + '/about',
				id: related_entities.article_ids[i]
				});
			new_article.fetch({
				success: _.bind(function(article) {
					this.add_article_view(article);
					}, this),
				error: _.bind(function(article) {
					this.add_error_view(article);
					}, this)
				});
				 
				// Update the number of articles loaded
				this.articles_loaded += 1;
				this.update_loading_feedback();
			}
		},
	add_article_view: function(article) {
		var view = new SingleArticle({model: article});
		},
	add_error_view: function(article) {
		var view = new SingleArticleError({model: article});
		$(this.el).append(view.render().el);
		},
	load_more_articles: function() {
		this.fetch_some_articles();
		},
	detect_scroll: function() {
		var trigger_point = 150;
		var current_position = this.el.scrollTop + this.el.clientHeight;
		var available_height = this.el.scrollHeight;
		// Only fire if the user has scrolled to the trigger_point
		if (current_position + trigger_point > available_height) {
			this.fetch_some_articles(this.load_step);
			}
		},
	update_loading_feedback: function() {
		if (this.articles_amt) {
			$('.articles-view .loaded').text(this.articles_loaded);
			$('.articles-view .total').text(this.articles_amt);
			if (this.articles_loaded == this.articles_amt) {
				$('.load-more').text('');
				$('.load-more').css('display', 'none');
				}
			else {
				$('.load-more').text('Scroll down to see more.');
				}
			}
		else {
			$('.load-more').text('No results.');
			$('.articles-view .loaded').text('0');
			$('.articles-view .total').text('0');
			}
		},
	reset_loading_feedback: function() {
		$('.articles-view .loaded').text('');
		$('.articles-view .total').text('');
		$('.load-more').css('display', 'block');
		$('.load-more').text('Loading...');
		},
	});

