InitialSearch = Backbone.View.extend({
	el: $('.initial-search'),
	active_search_domain: 'outcome',
	events: {
		'keypress #search-input': 'input_width',
		'click button': 'begin_search',
		'click .search-domain': 'change_search_domain',
		},
	initialize: function() {
		this.input = this.$('#search-input');
		 
		// Initialize empty autocomplete
		this.domain_entities = this.get_domain_entities(this.active_search_domain);
		$('#search-input').autocomplete({delay: 0, minLength: 3, source: this.domain_entities, disabled: false});
		
		this.set_cat_slider();
		},
	input_width: function(e) {
		// Make input width dynamic
		var span = $('.dynamic-width-helper');
		span.text(this.input.val()); 
		this.input.width(span.width());
		
		// If they press enter, do the search
		if (e.keyCode == 13) { this.begin_search(); }
		},
	begin_search: function(e) {
		if (!this.input.val()) return;
		
		var chosen_domain = this.active_search_domain;
		var value = this.input.val();
		
		var url = '/search#filters?' + chosen_domain + '=' + value
		window.location.href = url;
		},
	change_search_domain: function(event) {
		var clicked_on = event.target.id;
		// If they clicked on some category, 
		// switch the autocomplete domain to it
		if (clicked_on != this.active_search_domain) {
			this.$('.search-domain').removeClass('chosen');
			this.$('#' + event.target.id).addClass('chosen');
			this.active_search_domain = event.target.id;
			
			var chosen_text = clicked_on;
			switch (clicked_on) {
				case 'outcome': 
					chosen_text = 'an <span>outcome</span>';
					break;
				case 'exposure': 
					chosen_text = 'an <span>exposure</span>';
					break;
				case 'covariate': 
					chosen_text = 'a <span>covariate</span>';
					break;
				case 'population': 
					chosen_text = 'the <span>population</span>';
					break;
				case 'effectsize': 
					chosen_text = 'the <span>effect size</span>';
					break;
				case 'studydesign': 
					chosen_text = 'a <span>type of study</span>';
					break;
				case 'keyword': 
					chosen_text = '<span>mentioned</span> in the study';
					break;
				}
			this.$('.visible-cat span').html(chosen_text);
			
			if (clicked_on != 'keyword') {
				this.domain_entities = this.get_domain_entities(this.active_search_domain);
				this.input.autocomplete({disabled: false, source: this.domain_entities});
				}
			else {
				this.input.autocomplete({disabled: true});
				}
			}
		},
	get_domain_entities: function(cat) {
		var entities = [];
		$.ajax({
			url: '/api/entities',
			data: {requested_type: cat},
			dataType: 'json',
			async: false,
			success: function(data) {
				entities = data[cat];
				}
			});
		return entities;
		},
	set_cat_slider: function() {
		$('.epidem-categories').hover(function() {
			$('.dropdown-cats').slideDown(200);
			}, function() {
			$('.dropdown-cats').stop().slideUp(200);
			});
		}
	});

filters_list = new InitialSearch;
