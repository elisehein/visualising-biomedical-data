// This is the view for an individual filter
SingleFilter = Backbone.View.extend({
	tagName: 'li',
	className: 'filter',
	template: _.template($('#single-filter-template').html()),
	removalTemplate: _.template($('#filter-removal-template').html()),
	events: {
		'click .remove-filter': 'remove_filter',
		},
	initialize: function() {
		filters.bind('remove', this.verify_removal_option, this);
		filters.bind('add', this.verify_removal_option, this);
		filters.bind('reset', this.remove, this);
		},
	// Render this filter item
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		this.set_text();
		return this;
		},
	set_text: function() {
		var cat = this.model.get('epidem_category');
		var cat_text = '';
		
		// Take care of non human readable categories
		if (this.model.get('epidem_category') == 'studydesign') {
			cat_text = 'Type of study';
			}
		else if (this.model.get('epidem_category') == 'effectsize') {
			cat_text = 'Effect size';
			}
		else {
			cat_text = this.model.get('epidem_category');
			}
		
		if (this.model.get('is_keyword')) {
			cat = 'keyword';
		  cat_text = 'Keyword';
			}
		
		var val = this.model.get('value');
		var max_len = 21;
		this.$('.cat').text(cat_text);
		$(this.el).addClass(cat);
		if (val.length <= max_len) {
			this.$('.val').text(val);
			}
		else {
			this.$('.val').text(val.substring(0, max_len) + '...');
			}
		this.$('.val').attr('alt', val);
		},
	verify_removal_option: function() {
		// If there is more than one filter, add remove option
		if (filters.models.length > 1) {
			if (!$.contains($(this.el)[0], this.$('.remove-filter')[0])) {
				this.$('.val').append(this.removalTemplate);
				}
			}
		// If there is a single filter, clear remove option
		if (filters.models.length == 1) {
			$('.remove-filter').remove();
			}
		},
	remove_filter: function() {
		filters.remove(this.model); // Remove the model from filters
		this.remove();              // Remove the view
		}
	});

// This is the view combining all of the 
// individual filter views
FiltersList = Backbone.View.extend({
	el: $('#filters-view'),
	active_option: 'filtered-search-option',
	active_search_domain: 'keyword',
	ok_icon_template: _.template($('#ok-icon-template').html()),
	events: {
		'keypress #search-input': 'add_filter_model',
		'click .option': 'change_active_option',
		'click .search-domain': 'change_search_domain',
		},
	initialize: function() {
		filters.bind('add', this.add_filter_view, this);
		this.input = this.$('#search-input');
		this.build_initial_view();
		},
	build_initial_view: function() {
		
		// Initialize empty autocomplete
		$('#search-input').autocomplete({
			delay: 0, 
		  minLength: 3, source: [], 
		  disabled: true,
			position: {my: "left bottom", at: "left top"}
			});
		

		// Display filters
		var this_view = this;
		filters.each(function(f) {
			this_view.add_filter_view(f);
			});
		
		// Some UI magic
		this.reset_cat_slider();
		},
	add_filter_model: function(e) {
		if (e.keyCode != 13) return;
		if (!this.input.val()) return;
		
		var chosen_cat = '';
		var keyword = false;
		if (this.active_search_domain == 'keyword') {
			keyword = true;
			}
		else {
			chosen_cat = this.active_search_domain;
			}
		new_filter = new SearchParameter({
			epidem_category: chosen_cat,
			is_keyword: keyword,
			value: this.input.val(),
			});
		
		// If we want to start over with the search, remove all current filters first
		if (this.active_option == 'new-search-option') {
			filters.reset();
			}
		
		// Check this filter isn't a duplicate before adding it
		var is_duplicate = false;
		filters.each(function(f) {
			if (f.get('epidem_category') == new_filter.get('epidem_category')
				  && f.get('value') == new_filter.get('value')) {
				is_duplicate = true;
				}
			});
		
		if (!is_duplicate) {
			filters.add(new_filter);
			}
		
		this.input.val('');
		
		// Reload the filtered search option
		this.$('#filtered-search-option').click();
			
		},
	add_filter_view: function(filter) {
		var view = new SingleFilter({model: filter});
		$('#filters-list').append(view.render().el);
		view.verify_removal_option();
		},
	change_active_option: function(event) {
		// Only do something if this class is *not* chosen
		// (should only be able to choose currently *not* active state)
		var clicked_on = event.target.id;
		if (clicked_on != this.active_option) {
			this.$('.option').toggleClass('chosen');
			this.active_option = clicked_on;
			this.$('.option i').remove();
			this.$('#' + clicked_on).prepend('<i class="icon-ok"> </i>');
			}
		
		// Fade out current filters if we want to start a new search
		if (this.active_option == 'new-search-option') {
			this.$('.filter').addClass('inactive');
			}
		else {
			this.$('.filter').removeClass('inactive');
			}
		
		// Reset slider height
		this.reset_cat_slider();
		},
	change_search_domain: function(event) {
		var clicked_on = event.target.id;
		// If they clicked on some category, 
		// switch the autocomplete domain to it
		if (clicked_on != this.active_search_domain) {
			this.$('.search-domain').removeClass('chosen');
			this.$('#' + event.target.id).addClass('chosen');
			this.active_search_domain = event.target.id;
			
			var chosen_text = clicked_on;
			if (clicked_on == 'studydesign') { chosen_text = 'Study design'; }
			if (clicked_on == 'effectsize') { chosen_text = 'Effect size'; }
			this.$('.visible-cat span').text(chosen_text);
			
			if (clicked_on != 'keyword') {
				domain_entities = this.get_domain_entities(this.active_search_domain);
				this.input.autocomplete({disabled: false, source: domain_entities});
				}
			else {
				this.input.autocomplete({disabled: true});
				}
			}
		this.$('.dropdown-cats').hide();
		},
	get_domain_entities: function(cat) {
		var entities = [];
		$.ajax({
			url: '/api/entities',
			data: {requested_type: cat},
			dataType: 'json',
			async: false,
			success: function(data) {
				entities = data[cat];
				}
			});
		return entities;
		},
	reset_cat_slider: function() {
		$('.epidem-categories').hover(function() {
			$('.dropdown-cats').show();
			}, function() {
			$('.dropdown-cats').hide();
			});
		}
	});
