// Routing
// ==================================================================

RouteUpdater = Backbone.Router.extend({
	routes: {
		'': 'no_searches',
		},
	initialize: function() {
		filters.bind("add", this.add_query_param, this);
		filters.bind("remove", this.remove_query_param, this);
		filters.bind("reset", this.remove_all_params, this);
		Backbone.history.start({pushState: true});
		},
	no_searches: function(root, params) {
		console.log("router recognised me being on ''");
		},
	add_query_param: function(filter) {
		var current_params = URI(location.hash.substring(location.hash.indexOf('?')));
		var new_param = this.get_filter_key_val(filter);
		var new_params = current_params.addSearch(new_param.k, new_param.v).search();
		var new_hash = 'search#filters' + new_params;
		this.navigate(new_hash, {trigger: true});
		},
	remove_query_param: function(filter) {
		var current_params = URI(location.hash.substring(location.hash.indexOf('?')));
		var param_to_remove = this.get_filter_key_val(filter);
		var new_params = current_params.removeSearch(param_to_remove.k, param_to_remove.v).search();
		var new_hash = 'search#filters' + new_params;
		this.navigate(new_hash, {trigger: true});
		},
	remove_all_params: function() {
		this.navigate('search#filters?', { trigger: true });
		},
	get_filter_key_val: function(f) {
		var key = f.get('is_keyword')
			        ? 'keyword'
							: f.get('epidem_category');
		var val = f.attributes.value;
		return {k: key, v: val};
		}
	});


