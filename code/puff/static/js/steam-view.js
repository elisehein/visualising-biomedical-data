Steamgraph = Backbone.View.extend({
	tagName: 'div',
	className: 'steamgraph-container',
	template: _.template($('#steamgraph-template').html()),
			
	initialize: function(params) {
		related_entities.bind("fetch", this.remove, this);
		this.data = params.layers;
		},
			
	render: function(stack_offset) {
		
		// Clear area and reset template
		$(this.el).html(this.template);
		
		// For in-function use
		var this_view = this;
		
		// Get data
		var data = this.data,
		    m = data[0]['values'].length, // number of samples per layer
				years = _.map(data[0]['values'], function(o) { return o['year']; });
		
		// Set dimensions
	  var margin = {top: 20, bottom: 50, left: 20, right: 20}, 
				width = $('.steam-view').width(),
				height = 450;
		
		// This will give all data points a y-baseline;
		// used to make the whole stream vertically symmetric
		var stack = d3.layout.stack()
			.offset(stack_offset)
			.values(function(d) { return d.values; })
			.x(function(d) { return d.year; })
			.y(function(d) { return d.freq; });
		
		// Calculate a baseline for each data point
		var layers = stack(data);
					
		// Using time.scale() will guarantee that the domain is correct and uses years
		var x = d3.time.scale()
			.domain(d3.extent(data[0]['values'], function(d) { return d.year }))
			.range([0, width - 1]);
		
		// Get whole range of frequencies
		var freqs = [];
		_.each(layers, function(layer) {
			var this_freqs = _.map(layer['values'], function(o) { return o['freq'] + o['y0']; });
			freqs = freqs.concat(this_freqs);
			});
		
		var y = d3.scale.linear()
			.domain([0, d3.max(freqs)])
			.range([height - margin.top - margin.bottom, 0]);
		
		// This constructs the d attribute for our path
		var area = d3.svg.area()
			.interpolate('cardinal')
			.x(function(d) { return x(d.year);})
			.y1(function(d) { return y(d.y0 + d.freq);})   // This is used for the top line
			.y0(function(d) { return y(d.y0);}); 				   // This is used for the bottom line

		// Draw canvas
		var canvas = d3.select(this.$('.steamgraph')[0])
			.append("svg")
			.attr("width", width)
			.attr("height", height); // Add extra height for the axis
		
		// Add background
		var str_years = _.map(years, function(y) { return y.getFullYear().toString(); });
		var year_range = _.max(str_years) - _.min(str_years);
		this.add_bg(canvas, width, height - margin.bottom + margin.top, year_range);
		
		// Add steam
		var steam_top_distance = stack_offset == 'wiggle' ? margin.top : 2 * margin.top;
		canvas.selectAll("path")   // Declare that we are looking for path shapes
		.data(layers) 			       // Map our data to inexistent path shapes
		.enter().append("path")    // Realise a path shape
		.attr("d", function(d) { return area(d.values)})
		.attr('transform', 'translate(0,' + steam_top_distance + ')')
		.attr('class', 'steam')
		.style("fill", function() {  return this_view.random_blue(Math.floor(Math.random() * 5)); })
		//.style("fill", function(d) {  return this_view.get_cat_color(d.cat)});
		
		// Draw axis
		x_axis = d3.svg.axis()
			.scale(x)
			.orient('bottom')
			.ticks(d3.time.years, 2);
		
		canvas.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0, ' + (height - margin.bottom + margin.top) + ')')
			.call(x_axis);
			
		// Hover tooltips and colors
		this.add_hover_effects(canvas, x);
			
		// Activate vertical indicator
		this.activate_vertical_indicator();
			
		return this.el;
		},
		
	random_blue: d3.scale.linear()
		       .domain([0, 5])
					 .range(['#669acf', '#001b36', '#005ab6', '#102336', '#004183']),
	get_cat_color: function(cat) {
		switch(cat) {
			case 'outcome':
				return '#43777a';
				break;
			case 'exposure':
				return '#d95b45';
				break;
			case 'covariate':
				return '#442432';
				break;
			case 'population':
				return '#c02948';
				break;
			case 'studydesign':
				return '#b38d19';
				break;
			case 'effectsize':
				return '#807f87';
				break;
			default:
				return '#000';
				break;
			}
		},
	add_hover_effects: function(canvas, x) {
		// Add hover effects
		var this_view = this;
		canvas.selectAll('path.steam')
			.on('mousemove', function(d) {
				// Path name
				$('.steamgraph-container .tooltip').stop(true, true).fadeIn(200);
				$('.steamgraph-container .tooltip .val').text(d.name);
				$('.steamgraph-container .tooltip .cat').text(d.cat);
				
				// Path data
				var mousex = d3.mouse(this)[0]
				var mousey = d3.mouse(this)[1]
				var invertedx = x.invert(mousex);
				var year = invertedx.getFullYear();
				var freq = this_view.get_freq_for_year(d.values, year);
				$('.steamgraph-container .datatip .year').text(year);
				$('.steamgraph-container .datatip .freq').text(freq);
				$('.steamgraph-container .datatip').stop(true, true).show();
				var datatip_width = $('.steamgraph-container .datatip').width();
				$('.steamgraph-container .datatip').css('left', (mousex - datatip_width - 6) + 'px');
				$('.steamgraph-container .datatip').css('top', (mousey + 40) + 'px');
				
				// Path color
				$(this).css('opacity', 0.7);
				})
			.on('mouseout', function(d) {
				// Path name
				$('.steamgraph-container .tooltip').fadeOut(200);
				
				// Path data
				$('.steamgraph-container .datatip').hide();
				
				// Path color
				$(this).css('opacity', 1);
				});
		},
	
	get_freq_for_year: function(values, year) {
		var years = _.map(values, function(o) { return o['year'].getFullYear(); });
		var freqs = _.map(values, function(o) { return o['freq']; });
		var freq = freqs[years.indexOf(year)];
		return freq;
		},
	
	activate_vertical_indicator: function() {
		// Based on http://bl.ocks.org/WillTurman/4631136
		// By Will Turman
		d3.select(this.$('.steamgraph')[0])
			.on('mousemove', function() {
				var mousex = d3.mouse(this)[0] + 5;
				$('.vertical-indicator').css('left', mousex + 'px');
				})
			.on('mouseover', function() {
				mousex = d3.mouse(this)[0] + 5;
				$('.vertical-indicator').css('left', mousex + 'px');
				})
		},
	
	add_bg: function(canvas, width, height, range) {
		var bg = canvas.append('g')
			.attr('class', 'steam-bg')
			.attr('transform', 'translate(0, 0)')
			
		var step = width / range;
		var index = 0;
		for (x = 0; x < width; x += step) {
			var fill_color = index % 2 == '0' ? '#f9f9f9' : '#f4f4f4';
			var stroke_color = index % 3 == '0' ? '#dfdfdf' : '#f6f6f6';
			bg.append('rect')
			.attr('x', x)
			.attr('y', 0)
			.attr('width', step)
			.attr('height', height)
			.attr('stroke', stroke_color)
			.attr('fill', '#fff');
			index++;
			}
		},
			
	remove: function() {
		$(this.el).remove();
		}
	});


Steamgraphs = Backbone.View.extend({
	el: $('.steam-view'),
	stateTemplate: _.template($('#steamgraph-state-template').html()),
	msg_no_results: 'No results',
	msg_not_enough_results: 'Not enough matching articles to show results',
	msg_loading: '<span class="icon-spin icon-spinner"></span>',
	disabled_cids: [],
						
	events: {
		// Resetting will redraw everything using freq data from just filters
		'click .reset': 'reset',
		'click .toggle-baseline': 'toggle_baseline'
		},
						
	initialize: function() {
		related_entities.bind("fetch", this.show_loading, this);
		related_entities.bind("reset", this.decide_render, this);
		this.baseline = 'wiggle';
		},
						
	decide_render: function() {
		// Only render if we have some related entities
		if (related_entities.length) {
			this.render();
			}
		else {
			$('.steamgraphs').html(this.stateTemplate);
			$('.steamgraphs .state').text(this.msg_no_results);
			}
		},
	
	render: function() {
		this.get_data();
		this.draw_steamgraph();
		},
	
	get_data: function() {
		// Fetch frequency data
		this_view = this;
		this.freqs = [];
		filters.each(function(filter) {
			// Frequencies for entities are only available for non-keyword filters
			if (!filter.get('is_keyword')) {
				var new_freq = new EntityFrequencies({
					value: filter.get('value'),
					epidem_category: filter.get('epidem_category')
					});
				new_freq.fetch({
					data: {cat: new_freq.get('epidem_category'), val: new_freq.get('value')},
					dataType: 'json',
					async: false,
					success: function() {
						this_view.freqs.push(new_freq);
						}
					});
				}
			
			// If we only have one filter, then even if it is a keyword, the overall freqs will coincide
			else if (filter.get('is_keyword') && filters.length == 1) {
				overall = this_view.get_overall_freqs();
				this_view.freqs.push(overall);
				}
			});
		
		// If we have more than one filter,
		// get additional frequencies for combined filters
		if (filters.length > 1) {
			overall = this.get_overall_freqs();
			this.freqs.push(overall);
			}
		
		this.layers = this.prepare_data(this.freqs);
		},
	
	get_overall_freqs: function() {
		// Format ID list
		var ids = related_entities.article_ids.toString();
		
		var overall_freqs = new ArticleFrequencies();
		overall_freqs.fetch({
			type: 'POST',
			data: {'article_ids': ids},
			dataType: 'json',
			async: false,
			});
		return overall_freqs;
		},
	
	prepare_data: function(freqs) {
		// Put our freqs data in the right format for the steamgraph
		// This includes inserting 0 values for inexisting years
		
		// Find the combined range of all represented years
		var years = [];
		_.each(freqs, function(m) {
			var this_years = _.map(m.get('freqs'), function(o) { return o['year']; });
			years = years.concat(this_years);
			});

		var layers = [],
		    min_year = _.min(years), 
		    max_year = _.max(years);
		
		var this_view = this;
		
		// Add a layer for each set in our frequency data
		_.each(freqs, function(m) {
			var layer = this_view.create_layer(m, min_year, max_year);
			layers.push(layer);
			});
		
		return layers;
		},
	
	create_layer: function(o, min_year, max_year) {
		// We need to have our date in the right format for time.scale() to work
		var format = d3.time.format('%Y');
		
		var values = [],
				this_years = _.map(o.get('freqs'), function(o) { return o['year']; }),
				this_freqs = _.map(o.get('freqs'), function(o) { return o['freq']; }),
				current_year = min_year,
				name = o.has('value') ? o.get('value') : '',
				cat = o.has('epidem_category') ? o.get('epidem_category') : 'All filters',
				layer = { name: name,
									cat: cat,
									values: [] };
		for(current_year; current_year <= max_year; current_year++) {
			var val_obj = {},
					index = $.inArray(current_year, this_years);
			
			if (index > -1) {
				val_obj['freq'] = this_freqs[index];
				val_obj['year'] = format.parse(this_years[index].toString());
				}
			else {
				val_obj['freq'] = 0;
				val_obj['year'] = format.parse(current_year.toString());
				}
			values.push(val_obj);
			}
		layer['values'] = values;
		return layer
		},
	
	draw_steamgraph: function() {	
		// Data for less than 3 years will just render small points and is meaningless
		var years = _.map(this.layers[0]['values'], function(o) { return o['year']; });
		if (years.length > 3) {
			var steamgraph = new Steamgraph({layers: this.layers});
			$('.steamgraphs').html(steamgraph.render(this.baseline));
			}	
		else {
			$('.steamgraphs').html(this.stateTemplate);
			$('.steamgraphs .state').text(this.msg_not_enough_results);
			}
		},
	
	// For use by external objects; add a new entity layer and reload
	add_entity: function(cat, val) {
		
		// Check whether we already have steam for this entity
		var steam_exists = false;
		_.each(this.layers, function(layer) {
			if (layer['name'] == val && layer['cat'] == cat) {
				steam_exists = true;
				}
			});
		
		if (!steam_exists) {
			// Declare and fetch a new frequency model
			var new_freq = new EntityFrequencies({
				value: val,
				epidem_category: cat
				});
			var this_view = this;
			new_freq.fetch({
				url: '/api/entities/years',
				data: {cat: new_freq.get('epidem_category'), val: new_freq.get('value')},
				dataType: 'json',
				async: false,
				success: function() {
					this_view.freqs.push(new_freq);
					}
				});
			
			// Prepare the new layers
			this.layers = this.prepare_data(this.freqs);
			
			// Render the new steamgraph
			this.draw_steamgraph(this.baseline);
			}
		},
	
	reset: function() {
		// First enable the trends option in the cloud menus for non-filters
		_.each(this.disabled_cids, function(cid) {
			$('.cloud-menu#' + cid + ' .add-steam').removeClass('disabled');
			});
		
		// Now re-render
		this.decide_render();
		},
	toggle_baseline: function() {
		var button_text = this.baseline == 'wiggle' ? 'Centered view' : 'Traditional view';
		this.$('.toggle-baseline').text(button_text);
		this.baseline = this.baseline == 'wiggle' ? 'zero' : 'wiggle';
		this.draw_steamgraph();
		},
	show_loading: function() {
		$('.steamgraphs').html(this.stateTemplate);
		$('.steamgraphs .state').html(this.msg_loading);
		},
	});
