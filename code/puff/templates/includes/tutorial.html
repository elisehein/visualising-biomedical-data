<div class="row">
	<div class="threequarter centered">
		<h1>Guidelines: how to use <span style="font-family: 'Carrois Gothic SC'">EDViC</span>?</h1>
	</div>
</div>

<div class="row guidelines">
	<div class="threequarter centered">
		
		<!-- Contents -->
		<ul class="faq">
			<li>
				<a href="#search">Manipulating the search criteria</a>
				<ul>
				<li><a href="#filter-search">Drilling down the current search</a></li>
				<li><a href="#new-search">Starting a new search</a></li>
				<li><a href="#delete-filter">Deleting a filter</a></li>
				</ul>
			</li>
			<li><a href="#articles">Exploring the list of matching articles</a></li>
			<li>
				<a href="#cloud">Interacting with the cloud of related concepts</a>
				<ul>
				<li><a href="#warning">Warning messages</a></li>
				<li><a href="#hiding-cats">Hiding and showing epidemiological categories</a></li>
				<li><a href="#context-menu">Cloud interaction menu</a></li>
				</ul>
			
			</li>
			<li><a href="#steam">Understanding the frequency of concept occurrence graph</a>
				<ul>
				<li><a href="#reading-steam">How to read the graph?</a></li>
				</ul>
			</li>
		</ul>
		
		<!-- Using the search bar -->
		
		<div class="row container" id="search">
			<h2>Using the search bar</h2>
			<div class="threequarter">
				<p>
				Having reached the results page you may well want to alter your search. 
				This can be done using the blue search bar at the bottom of
				the page, which also serves as a reminder of the current search criteria:
				each blue "label" shows the epidemiological category and the specific concept of that search term.
				</p>
				<ul>
					<li id="filter-search">To drill down your search with further epidemiological concepts, 
					simply fill in the empty search label at the end of the list. Choose
					the epidemiological type by hovering over the category indicator,
					and type the concept where it says <span>"Refine your search"</span>. Press enter to search.
					Use the suggestions given as you type to ensure the best possible results.</li>
					<li id="new-search">To start over with a completely new search, clearing all existing labels, 
					choose the <span>"New search"</span> option next to the search label.</li>
					<li id="delete-filter">To clear individual search terms, click the delete icon on the appropriate label.</li>
				</ul>
			</div>
			<div class="quarter">
			</div>
		</div>
		
		<!-- Articles -->
		<div class="row">
			<h2>Exploring matching articles</h2>
			<div class="threequarter">
				<p>The list of articles on the left side of the results page holds
				all publications matching the current search criteria. The numbers
				next to the heading indicate how many there are in total, and how
				many are loaded in the list. Simply scroll down to keep loading more
				articles until all are shown.
				</p>
				<p>
				Click on any article to see a more detailed view, including the
				abstract with relevant highlights, and a table showing the epidemiological
				specifics of that particular study.
				</p>
			</div>
			<div class="quarter">
			</div>
		</div>
		
		<!-- Cloud -->
		<div class="row" id="cloud">
			<h2>The cloud of related concepts</h2>
			<div class="threequarter">
				<p>
				The term cloud visualises your search results - it shows concepts
				that most popularly occur in the publications matching your search criteria
				(the more popular the concept, the bigger the word). The terms are color 
				coded based on their epidemiological category (see the legend above the
				cloud), and each category has it's own rotation factor to aid scannability.
				</p>
				<p id="warning">
				With large result sets, there may be too many terms to fit in the cloud 
				while still having a sensible size. In cases like these, a small message
				will appear above the cloud and you can either hide or remove some terms
				or categories to make room for others that you might be more interested in.
				However, a large set of results isn't useful anyway in terms of going into details,
				and not all terms need to be visible in order to get a good general overview
				of a concept. For a more detailed visualisation where all terms are visible,
				the search should be more specific -- adding filters might help!
				</p>
				
				<h3 id="hiding-cats">Hiding epidemiological categories</h3>
				<p>
				When the cloud first loads, only the exposures and outcomes of matching 
				studies are visible, as these are the most relevant. To see other epidemiological
				characteristics, use the legend above the cloud to hide or unhide a category.
				The terms that have been hidden are still accessible from the <span>"Hidden terms</span>
				dropdown -- click on any term in that dropdown to bring it back to the cloud.
				</p>
				
				<h3 id="context-menu">Interacting with the cloud</h3>
				
				<p>
				By clicking on any term in the cloud, a small menu appears for actions to
				take regarding that specific term. You can see the full term that was clicked on in case it had been concatenated, as well as the terms epidemiological type and UMLS category. The options in the menu are:
				
				<ul class="no-bullets">
					<li><span class="li-title"><i class="icon-filter"> </i>Add this as a filter</span>
					This will filter the current search with the chosen term, adding a label for it on the search bar.
					</li>
					<li><span class="li-title"><i class="icon-search"> </i>Start a new search</span>
					This will clear all current search criteria and begin a new search with the chosen term.
					</li>
					<li><span class="li-title"><i class="icon-eye-close"> </i>Hide from the results</span>
					This will hide the individual term from the cloud in order to avoid distractions,
					adding it to the hidden terms dropdown.
					</li>
					<li><span class="li-title"><i class="icon-bar-chart"> </i>Compare trends</span>
					This will add an additional dataset to the steamgraph (see <a href="#steam">Frequencies of occurrence</a>) to be able to compare the publication frequencies of the concepts in the current search criteria to that of the chosen term.
					</li>
					<li><span class="li-title"><i class="icon-book"> </i>See context</span>
					This will open a small modal with a list of sentences from the related publications
					where this term occurs in, with the term highlighted and color coded based on its epidemiological category.
					</li>
					<li><span class="li-title"><i class="icon-google-plus"> </i>Browse external sources</span>
					For now, this will perform a Google Scholar search in a new tab with the chosen term.
					</li>
					
				</ul>
				</p>
				
			</div>
			<div class="quarter">
			</div>
		</div>
		
		<!-- Steam -->
		<div class="row" id="steam">
			<h2>Understanding the frequencies of concept occurrence</h2>
			<div class="threequarter">
				<p>
				The steamgraph under the cloud is a visualisation of the popularity of the concepts 
				in the current search criteria in biomedical publications. By default, it 
				shows the popularity of all search terms separately, and the popularity of publications
				where all of these terms occur together (this is usually a considerably smaller
				bar of steam and more difficult to notice than the others). However, other
				concepts can be added to the graph for comparison as well -- see <a href="#context-menu">Interacting with the cloud</a>. The graph can be restored to its original view by clicking the button <span>"Redraw"</span>
				</p>
				
				<h3 id="reading-steam">How to read the graph</h3>
				
				<p>
				At it's default view, the graph is a collection of flowing bars of "steam" -- hence the name, steamgraph. 
				The publication popularity is expressed by the volume of any one bar: the thicker the bar at
				a point in time, the more popular the concepts it is representing were at that time.
				If a more traditional view is preferred, there is a corresponding button to toggle the position of the steam bars.
				</p>
				<p>
				Hovering over each individual steam bar will show the year and number of publications at that point. It is important to note
				that most steamgraphs will appear a lot "fatter" towards the 2000's; this is simply because there is not a lot of data before the 90s. Therefore more significant peaks should be considered interesting; a steadily rising steambar usually indicates a relatively uniform distribution.
				</p>
				
			</div>
			<div class="quarter">
			</div>
		</div>
		
		
	</div>
</div>
