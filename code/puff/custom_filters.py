#!/usr/bin/python
# -*- coding: utf-8 -*-

from puff import app

@app.template_filter('querystring_spacing')
def suitable_querystring_spacing(s):
    return '%20'.join(s.split(' '))

# Convert a list of unicode dicts into a list of ascii dicts
@app.template_filter('list_of_unicode_dicts_to_ascii')
def list_of_unicode_dicts_to_ascii(l):
    new_list = []
    print l
    
    for d in l:
        new_dict = unicode_dict_to_ascii(d)
        new_list.append(new_dict)
    
    return new_list

@app.template_filter('unicode_dict_to_ascii')
def unicode_dict_to_ascii(d):
    print "ENCODING DICT"
    print d
    
    ascii_dict = dict()
    for k, v in d.items():
        try:
            ascii_dict[str(k)] = str(v)
        except UnicodeEncodeError:
            ascii_dict[str(k)] = v.encode("utf8")
    
    return ascii_dict

@app.template_filter('unicode_list_to_ascii')
def unicode_list_to_ascii(l):
    return [str(item) for item in l]

    
