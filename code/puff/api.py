from puff import app
from flask import jsonify, request, Response
import simplejson as json
from database import Database
from utils import EPIDEM_TYPES

db = Database()

# SEARCH RESOURCES
#####################################################################

# This is for the autocorrect function
@app.route('/api/entities')
def display_all_entities():
    requested_type = request.args.get('requested_type')
    entities = db.get_all_entities(requested_type=requested_type)
        
    return jsonify(entities)

# WORD CLOUD RESOURCES
#####################################################################

# related_entity_type is used to differentiate between entities that are 
# classified as different epidemiological characteristics in
# different cases. Can be any of the following:
#    outcome
#    exposure
#    covariate
#    studydesign
#    population
#    effectsize


@app.route('/api/entities/related')
def display_related_entities():
    # Get all of the filters
    filters = request.args.to_dict(flat=False)
    retmax = filters.pop('retmax', None)
    requested_types = filters.pop('requested_cat', None)
     
    # Only proceed if we actually have filters
    if len(filters):
        if not requested_types: 
            requested_types = EPIDEM_TYPES
            
        related = db.get_combined_related_entities(filters, retmax=retmax,
                                                   requested_types=requested_types)
        
        return jsonify(related)
    else:
        return jsonify({'article_ids': [], 'related': []})

            
# FREQUENCY RESOURCES
#################################################################### 

@app.route('/api/entities/years')
def display_years_for_keyword():
    given_type = request.args.get('cat')
    given_term = request.args.get('val')
    years = db.get_years_for_keyword(given_term, given_type=given_type)
    return jsonify({'freqs': years})

@app.route('/api/articles/years', methods=['POST'])
def display_years_for_articles():
    # Get article ids
    year_freqs = [];
    if (request.form.has_key('article_ids')):
        article_ids = request.form['article_ids']
        year_freqs = db.get_years_for_articles(article_ids)
        
    return jsonify({'freqs': year_freqs})


# ARTICLE INFO RESOURCES
#####################################################################

@app.route('/api/article/<article_id>/about')
def display_article_about(article_id):
    info = db.get_article_info(article_id)
    # Common abbreviations:
    #   AB     Abstract
    #   AD     Affiliation
    #   AU     Author(s) -- returns list
    #   DEP    Date of electronic publication
    #   DP     Date published
    #   FAU    Full author(s)
    #   IP     Issue 
    #   IS     ISSN
    #   JT     Genome Biology
    #   LA     Language
    #   OWN    Owner
    #   PG     Pagination
    #   PHST   Publication history status
    #   PMC    Pubmed central identifier
    #   PMID   Pubmed unique identifier
    #   PT     Publication type
    #   SO     Source
    #   TA     Journal title abbreviation
    #   TI     Title
    #   VI     Volume
    # Meanings of all abbreviations available on
    # http://biopython.org/DIST/docs/api/Bio.Medline-pysrc.html
    return jsonify(**info)

@app.route('/api/article/<article_id>/brief')
def display_article_brief(article_id):
    brief = db.get_article_brief(article_id)
    return jsonify({'brief': brief})

@app.route('/api/article/<article_id>/facts')
def display_article_key_facts(article_id):
    facts = db.get_article_key_facts(article_id)
    return jsonify(facts)

@app.route('/api/article/<article_id>/highlights')
def display_article_highlights(article_id):
    highlights = db.get_article_highlights(article_id)
    return jsonify({'highlights': highlights})

@app.route('/api/entity/context', methods=['POST'])
def display_entity_context():
    context = []
    if request.form.has_key('concept'):
        concept = request.form['concept']
        article_ids = request.form['article_ids']
        context = db.get_sentences_in_articles_containing(concept, article_ids)
    return jsonify({'context': context})

