class hashable_dict(dict):
    def __init__(self, key, *args, **kwargs):
        self.key = key
        dict.__init__(self, *args, **kwargs)
    def __hash__(self):
        return hash(self.get(self.key))
            
# Define server name
####################################################################

SERVER_NAME = '127.0.0.1'
SERVER_PORT = '5000'

# Define all epidemiological types aka table names
#####################################################################
EPIDEM_TYPES = ['outcome', 'exposure', 'covariate', 'studydesign', 'population', 'effectsize']
ALL_EPIDEM_TYPES = ['outcome', 'exposure', 'covariate', 'studydesign',
                    'population', 'effectsize']

# Get consistent naming of db fields
#####################################################################
DB_NAMING = {}
for type in ALL_EPIDEM_TYPES:
    DB_NAMING[type] = {'entity': '', 'article_id': '', 'year': 'year', 'other_fields': []}

# Populate 
DB_NAMING['outcome']['entity'] = 'outcome'
DB_NAMING['outcome']['article_id'] = 'pmid'
DB_NAMING['outcome']['other_fields'] = ['other_outcome as other_value', 'umls_group',
                                        'umls_category']

DB_NAMING['exposure']['entity'] = 'exposure'
DB_NAMING['exposure']['article_id'] = 'pmid'
DB_NAMING['exposure']['other_fields'] = ['other_exposure as other_value', 'umls_group', 'umls_category']

DB_NAMING['covariate']['entity'] = 'covariate'
DB_NAMING['covariate']['article_id'] = 'pmid'
DB_NAMING['covariate']['other_fields'] = ['other_covariate as other_value', 'umls_group', 'umls_category']

DB_NAMING['studydesign']['entity'] = 'type_of_study'
DB_NAMING['studydesign']['article_id'] = 'pmid'
DB_NAMING['studydesign']['other_fields'] = ['others_study_design as other_value',
                                         'treatment_response', 'time_attribute']

DB_NAMING['population']['entity'] = 'population'
DB_NAMING['population']['article_id'] = 'pmid'
DB_NAMING['population']['other_fields'] = ['other_population as other_value', 'age_stage',
                                           'gender', 'nationality', 'ethnicity']

DB_NAMING['effectsize']['entity'] = 'effect_size_concept'
DB_NAMING['effectsize']['article_id'] = 'pmid'
DB_NAMING['effectsize']['other_fields'] = ['effect_size_type as other_value', 'effect_size_number']

print DB_NAMING
