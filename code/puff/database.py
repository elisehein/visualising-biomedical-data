from Bio import Entrez, Medline
from pattern.en import pluralize
from utils import EPIDEM_TYPES, ALL_EPIDEM_TYPES, DB_NAMING
from copy import deepcopy
from flask import g

# The database wrapper class to provide querying functions
class Database():
    
    # Set up the connections
    def __init__(self):
        
        # Tell pubmed who we are
        Entrez.email = "elisehein@gmail.com"
        
        
    # Getting resources for the search funtion 
    #################################################################
    
    # Get all entities mentioned in the db, organised by category
    def get_all_entities(self, requested_type=None):
        
        # Declare entity types storage
        entities = {}
        
        requested_types = EPIDEM_TYPES
        if requested_type:
            requested_types = [requested_type]
        
        for entity_type in requested_types:
            entity_type_table = self._get_correct_db_fields(entity_type)
            q = '''select distinct %s 
                   from %s 
                   where %s != ""''' % (entity_type_table['entity'], 
                                        entity_type, 
                                        entity_type_table['entity'])
            g.c.execute(q)
            result = g.c.fetchall()
            entities[entity_type] = map(lambda x:
                                              x.get(entity_type_table['entity']),
                                              result)
            
        return entities
    
    # Match articles the contain a given keyword 
    # (can specify field=title)
    # and return the corresponding ids
    def get_articles_containing_keyword(self, search_term, retmax=None, field=None):
        
        ids = []
        
        # Restrict the number of results
        if retmax:
            handle = Entrez.esearch(db="pubmed", term=search_term, retmax=retmax, field=field)
            ids = Entrez.read(handle)['IdList']
            handle.close()
        
        # Get all results
        # This shouldn't be used, takes forever
        else:
            # First find out how many results there are
            handle = Entrez.esearch(db="pubmed", term=search_term, retmax=0,
                                    field=field)
            results = Entrez.read(handle)['Count']
            handle.close()
            
            # Now retrieve all of them
            handle = Entrez.esearch(db="pubmed", term=search_term, retmax=results,
                                   field=field)
            ids = Entrez.read(handle)['IdList']
            handle.close()
    
        return ids
    
    
    # Getting related entities
    #################################################################
    
    # Return combined related entities
    def get_combined_related_entities(self, filters, retmax=None,
                                      requested_types=EPIDEM_TYPES):
        
        # Find a list of matching article ids for each filter we have
        article_ids = list()
        keywords = filters.pop('keyword', [])
        
        # Keywords need to be treated without a type
        for term in keywords:
            article_ids.append(set(
                self._get_related_articles_with_term(term, retmax=5000)))
                
        # Terms with a given_type
        for given_type, search_terms in filters.iteritems():
            for term in search_terms:
                article_ids.append(set(
                    self._get_related_articles_with_term(term, given_type=given_type)))
                    
        # Find the intersection of all the sets in this list
        # This gives us article id's where all the given terms occur
        intersection = list(reduce(lambda x, y: x & y, article_ids,
                                   article_ids[0]))
        
        # No need to proceed if we find no article ids
        # (we'll encounter an sql syntax error anyway with  an empty list)
        related = list()
        if intersection:
            
            # Since retmax is called over and over, need to normalise
            if retmax and len(filters) > 0:
                retmax = int(int(retmax) / len(filters))
            
            for type in requested_types:
                related_to_type = self._get_related_entities_of_type(
                    intersection, type, retmax=retmax)
                related.extend(related_to_type)
        
        return {'related': related, 'article_ids': intersection}
        
    # Get a list of articles where the search term appears
    # If a given_type is not specified, treat as a keyword search
    # that uses PubMed
    def _get_related_articles_with_term(self, given_term, given_type=None,
                                        retmax=None):
        if given_type == None:
            # First find all articles where the term appears in the title
            ids_intitle = self.get_articles_containing_keyword(given_term,
                                                               field="title",
                                                               retmax=retmax)
            
            # If this was not enough to satisfy retmax, add search results from
            # article contents
            ids_incontent = list()
            if retmax != None and len(ids_intitle) < retmax:
                no_of_ids_short = retmax - len(ids_intitle)
                ids_incontent = self.get_articles_containing_keyword(given_term,
                                                                     retmax=no_of_ids_short)
                
            article_ids = ids_intitle + ids_incontent
            return article_ids
            
        else:
            # Normalise naming
            given_type_table = self._get_correct_db_fields(given_type)
            
            # Build and execute query 
            q = '''select %s from %s where %s="%s"''' % \
            (given_type_table['article_id'], given_type,
             given_type_table['entity'], given_term)
            
            
            g.c.execute(q)
            query_result = g.c.fetchall()
            
            # Return a LIST of ids
            article_ids = map(lambda x: str(x[given_type_table['article_id']]),
                              query_result)
            return article_ids
            
    # Return all entities from requested tables that match an article id
    def _get_related_entities_of_type(self, article_ids_list, requested_type, retmax=None):
        # Declare related entities list
        related_entities = [] 
        
        # Make article_ids list sql IN clause compatible
        article_ids = ', '.join(article_ids_list)
    
        # Normalise naming
        requested_type_table = self._get_correct_db_fields(requested_type)
        
        # First refine selection: some will need to contain umls info
        selection = list()
        selection.append(requested_type_table['entity'])
        if requested_type in ['covariate', 'outcome', 'exposure']:
            selection.extend(['umls_group', 'umls_category'])
        if requested_type in ['population']:
            selection.extend(['other_population'])
        
        q = '''select %s, count(*) as freq 
               from %s where %s in (%s) and %s != ""
               group by %s order by freq desc limit 200''' % (",".join(selection),
                                                              requested_type,
                                                              requested_type_table['article_id'],
                                                              article_ids,
                                                              requested_type_table['entity'],
                                                              requested_type_table['entity'])
        # Add limit if there is one
        if retmax:
            q += ''' limit %s''' % str(retmax[0])
            
        g.c.execute(q)
        related_entities = g.c.fetchall()
        
        # Finally!
        return self._consistent_repr_of_dicts(related_entities)
    
    def _consistent_repr_of_dicts(self, related):
        new_related_list = ();
        for d in related:
            new_d = {}
            new_d['freq'] = d['freq']
            for k in self._get_all_entity_fields(): 
                if k in d.keys():
                    new_d['epidem_category'] = k
                    new_d['value'] = d[k]
            new_d['other_details'] = {}
            for k in self._get_all_other_fields():
                for k2 in k:
                    if k2 in d.keys():
                        new_d['other_details'][k2] = d[k2]
            
            new_related_list = new_related_list + (new_d,)
        return new_related_list
    
    # This is so I can use consistency in my queries
    # Requested types:
    #    entity: main entity field in table
    #    article_id: article id field in table
    def _get_correct_db_fields(self, table):
        # Need to make a copy, otherwise will pop items off the original
        tmp_db_naming = deepcopy(DB_NAMING)
        return tmp_db_naming[table]
    
    def _get_all_entity_fields(self):
        tmp_entity_fields = []
        for k, v in deepcopy(DB_NAMING).items():
            tmp_entity_fields.append(v['entity'])
        return tmp_entity_fields
    
    def _get_all_other_fields(self):
        tmp_other_fields = []
        for k, v in deepcopy(DB_NAMING).items():
            tmp_other_fields.append(v['other_fields'])
        return tmp_other_fields
    
    # Getting information about frequencies
    #################################################################
    
    # Return a list of years when an entity has occurred in published papers
    def get_years_for_keyword(self, keyword, given_type=None):
        
        # If we have a given type, we're dealing with an entity
        # and can look for year info in the db
        if given_type:
            fields = self._get_correct_db_fields(given_type)
            q = '''select %s, count(*) as freq 
                   from %s where %s="%s" and %s != 0 and %s != -1 and %s != ''
                   group by %s order by %s asc''' % (fields['year'],
                                                      given_type,
                                                      fields['entity'],
                                                      keyword,
                                                      fields['year'],
                                                      fields['year'],
                                                      fields['year'],
                                                      fields['year'],
                                                      fields['year'])
            g.c.execute(q)
            query_result = g.c.fetchall()
            
            return query_result
            
        # If we don't have a given type, we should do a keyword search
        # and find dates from the article info api function
        else:
            article_ids = self.get_articles_containing_keyword(keyword,
                                                               retmax=10)
            years = []
            for id in article_ids:
                article_info = self.get_article_info(id)
                
                # Could use python dateutil, but it fails in some cases
                # even with fuzzy matching. Just take the first 4 chars --
                # usually this is the year
                years.append(int(article_info['DP'][0:4]))
                
            # Find frequencies for each year
            # {year: freq, year: freq}
            freqs = {}
            for year in years:
                freqs[year] = freqs.get(year, 0) + 1
            
            return [{'year': year, 'freq': freq} for year, freq in freqs.items()]
        
    # Return a list of years with frequencies for a list of articles
    def get_years_for_articles(self, article_ids):
        
        q = '''select year, count(*) as freq 
               from pmid where year != "" and year != 0 and year != -1 and pmid in (%s)
               group by year order by year asc''' % article_ids
        g.c.execute(q)
        freqs = g.c.fetchall()
        
        return freqs
            
    
    # Getting information about a specific article
    #################################################################
    
    # Return article abstract, date, author etc
    def get_article_info(self, article_id):
        handle = Entrez.efetch(db='pubmed', id=article_id, rettype='medline', retmode='text')
        records = Medline.parse(handle)
        records = list(records)
        return records[0]
    
    # Return a brief of the article with all information to use with the
    # highlights table
    def get_article_brief(self, article_id):
        handle = Entrez.efetch(db='pubmed', id=article_id, rettype='abstract', retmode='text')
        brief = handle.read()
        return brief
    
    # Return epidemiological info of a specific article
    def get_article_key_facts(self, article_id):
        
        # Initialize list of dicts
        # Each dict will contain information from one table
        facts = {}
        
        # For each db table, get relevant information 
        # and store it in the facts list
        for table in ALL_EPIDEM_TYPES:
            
            # Get the correct naming for this table
            fields = self._get_correct_db_fields(table)
            article_id_field = fields.pop('article_id')
            value_field = fields.pop('entity')
            fields.pop('year')
            
            # The selection should include all relvant fields
            other = fields.pop('other_fields', [])
            selection = value_field + ' as value, '
            selection += ", ".join(other)
            
            q = '''select %s
                   from %s
                   where %s = %s and %s != ""''' % (selection, table, 
                                                    article_id_field, 
                                                    article_id, value_field)
            g.c.execute(q)
            query_result = g.c.fetchall()
            facts[pluralize(table)] = query_result
        
        return facts
    
    # Return a list of relevant substrings in the article abstract
    def get_article_highlights(self, article_id):
        q = '''select distinct concept, class from highlights where pmid = %s''' % article_id
        g.c.execute(q)
        highlights = g.c.fetchall()
        #highlights = map(lambda x: x['concept'], query_result)
        return highlights
        
    # Get a list of sentences where a given concept occurs in a set of articles
    def get_sentences_in_articles_containing(self, concept, article_ids):
        
        # Get relevant highlight rows
        q = '''select pmid, concept, class, start, end from highlights
               where pmid in (''' + article_ids + ''') 
               and concept like "%''' + concept + '''%"'''
                            
        g.c.execute(q)
        results = g.c.fetchall();
        
        # Get the briefs
        briefs = {}
        article_ids_list = article_ids.split(',')
        for id in article_ids_list:
            brief = self.get_article_brief(id)
            briefs[id] = brief
            
        # Get sentences
        sentences = []
        for highlight in results:
            # Get the correct brief where this highlight occurs
            corresp_brief = briefs[str(highlight['pmid'])]
            
            # Find the right sentence in this brief
            # The start and end ints are error prone, but more or less stay
            # within the boundaries of the word. Therefore just find one char
            # and it will probably be in the correct sentence
            index = highlight['start']
            
            sentence_start = 0
            sentence_end = len(corresp_brief)
            # Go back in the absract until we find the beginning of the sentence
            for i in range(index, 0, -1):
                current_char = corresp_brief[i]
                if current_char in ['.', '!', '?']:
                    sentence_start = i + 2
                    break;
                    
            # Go forward in the abstract until we find the end of the sentence
            for i in range(index, len(corresp_brief)):
                current_char = corresp_brief[i]
                if current_char in ['.', '!', '?']:
                    sentence_end = i + 1
                    break;
                    
            sentence = {}
            sentence['sentence'] = corresp_brief[sentence_start:sentence_end]
            sentence['highlight'] = highlight['concept']
            sentence['highlight_cat'] = highlight['class']
            sentences.append(sentence)
        
        return sentences
