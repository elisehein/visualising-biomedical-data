from flask import Flask
from flask import g
import MySQLdb
import MySQLdb.cursors
import os

app = Flask(__name__)
app.config.from_object('config')

# DB credentials
password = app.config['DB_PASS']
username = app.config['DB_USER']
db_name = app.config['DB_NAME']
    
@app.before_request
def before_request():
    # Connect to entity data
    g.db = MySQLdb.connect(host='localhost', user=username,
                              passwd=password, db=db_name,
                              cursorclass=MySQLdb.cursors.DictCursor)
    g.c = g.db.cursor()
    
@app.teardown_request
def teardown_request(exception):
    # Disconnect
    g.c.close()
    g.db.close()
    
import puff.views
import puff.api
import puff.custom_filters

